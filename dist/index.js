"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.compare = compare;
exports.isArray = isArray;
exports.isBlob = isBlob;
exports.isBoolean = isBoolean;
exports.isDate = isDate;
exports.isEvent = isEvent;
exports.isFile = isFile;
exports.isFloat = isFloat;
exports.isFormData = isFormData;
exports.isFunction = isFunction;
exports.isInteger = isInteger;
exports.isJSON = isJSON;
exports.isNaN = isNaN;
exports.isNotEmptyArray = isNotEmptyArray;
exports.isNotEmptyObject = isNotEmptyObject;
exports.isNotEmptyString = isNotEmptyString;
exports.isNull = isNull;
exports.isNumber = isNumber;
exports.isObject = isObject;
exports.isRegExp = isRegExp;
exports.isString = isString;
exports.isUndefined = isUndefined;
exports.default = void 0;

var _formData = _interopRequireDefault(require("form-data"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function compare(firstValue, secondValue) {
  if (typeof firstValue !== typeof secondValue) {
    return false;
  }

  if (isArray(firstValue) && isArray(secondValue)) {
    if (firstValue.length !== secondValue.length) {
      return false;
    }

    for (var i = 0; i < firstValue.length; i++) {
      if (!compare(firstValue[i], secondValue[i])) {
        return false;
      }
    }
  } else if (isObject(firstValue) && isObject(secondValue)) {
    var firstKeys = Object.keys(firstValue),
        secondKeys = Object.keys(secondValue);

    if (firstKeys.length !== secondKeys.length) {
      return false;
    }

    for (var _i = 0; _i < firstKeys.length; _i++) {
      if (firstKeys[_i] !== secondKeys[_i]) {
        return false;
      }

      if (!compare(firstValue[firstKeys[_i]], secondValue[secondKeys[_i]])) {
        return false;
      }
    }
  } else if (isJSON(firstValue) && isJSON(secondValue)) {
    try {
      var first = JSON.parse(firstValue),
          second = JSON.parse(secondValue);

      if (!compare(first, second)) {
        return false;
      }
    } catch (e) {
      return false;
    }
  } else if (isNaN(firstValue) && !isNaN(secondValue) || !isNaN(firstValue) && isNaN(secondValue)) {
    return false;
  } else if (firstValue !== secondValue) {
    return false;
  }

  return true;
}

function isArray(value) {
  return Array.isArray(value) === true;
}

function isBlob(value) {
  return (isObject(value) && isNumber(value.size) && isString(value.type) && isFunction(value.slice)) === true;
}

function isBoolean(value) {
  return typeof value === 'boolean';
}

function isDate(value) {
  return (value instanceof Date && !isNaN(value.valueOf())) === true;
}

function isEvent(value) {
  return !!(value && value.stopPropagation && value.preventDefault);
}

function isFile(value) {
  return (isBlob(value) && (isObject(value.lastModifiedDate) || isNumber(value.lastModified)) && isString(value.name)) === true;
}

function isFloat(value) {
  return (isNumber(value) && !isInteger(value) && Number.parseInt(value.toString()) !== Number.parseFloat(value.toString())) === true;
}

function isFormData(value) {
  return value instanceof _formData.default;
}

function isFunction(value) {
  return typeof value === 'function';
}

function isInteger(value) {
  return (isNumber(value) && Number.isInteger(value) && Number.parseInt(value.toString(), 10) === value && Number(value) % 1 === 0) === true;
}

function isJSON(value) {
  if (isString(value)) {
    try {
      var json = JSON.parse(value);
      return (isObject(json) || isArray(json)) === true;
    } catch (e) {}
  }

  return false;
}

function isNaN(value) {
  return Number.isNaN(Number(value));
}

function isNotEmptyArray(value) {
  return (isArray(value) && value.length > 0) === true;
}

function isNotEmptyObject(value) {
  return (isObject(value) && Object.keys(value).length > 0) === true;
}

function isNotEmptyString(value) {
  return (isString(value) && value.replace(/\s/g, '').length > 0) === true;
}

function isNull(value) {
  return null === value;
}

function isNumber(value) {
  return typeof value === 'number';
}

function isObject(value) {
  return (typeof value === 'object' && !isArray(value) && !isNull(value) && !isFunction(value)) === true;
}

function isRegExp(value) {
  return Object.prototype.toString.call(value) === '[object RegExp]';
}

function isString(value) {
  return typeof value === 'string';
}

function isUndefined(value) {
  return typeof value === 'undefined';
}

var DataValidator = {
  compare,
  isArray,
  isBlob,
  isBoolean,
  isDate,
  isEvent,
  isFile,
  isFloat,
  isFormData,
  isFunction,
  isInteger,
  isJSON,
  isNaN,
  isNotEmptyArray,
  isNotEmptyObject,
  isNotEmptyString,
  isNull,
  isNumber,
  isObject,
  isRegExp,
  isString,
  isUndefined
};
var _default = DataValidator;
exports.default = _default;