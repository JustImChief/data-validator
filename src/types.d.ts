declare function compare(firstValue: any, secondValue: any): boolean;

declare function isArray(value: any): boolean;

declare function isBlob(value: any): boolean;

declare function isBoolean(value: any): boolean;

declare function isDate(value: any): boolean;

declare function isEvent(value: any): boolean;

declare function isFile(value: any): boolean;

declare function isFloat(value: any): boolean;

declare function isFormData(value: any): boolean;

declare function isFunction(value: any): boolean;

declare function isInteger(value: any): boolean;

declare function isJSON(value: any): boolean;

declare function isNaN(value: any): boolean;

declare function isNotEmptyArray(value: any): boolean;

declare function isNotEmptyObject(value: any): boolean;

declare function isNotEmptyString(value: any): boolean;

declare function isNull(value: any): boolean;

declare function isNumber(value: any): boolean;

declare function isObject(value: any): boolean;

declare function isRegExp(value: any): boolean;

declare function isString(value: any): boolean;

declare function isUndefined(value: any): boolean;

declare type DataValidator = {
  compare(firstValue: any, secondValue: any): boolean;
  isArray(value: any): boolean;
  isBlob(value: any): boolean;
  isBoolean(value: any): boolean;
  isDate(value: any): boolean;
  isEvent(value: any): boolean;
  isFile(value: any): boolean;
  isFloat(value: any): boolean;
  isFormData(value: any): boolean;
  isFunction(value: any): boolean;
  isInteger(value: any): boolean;
  isJSON(value: any): boolean;
  isNaN(value: any): boolean;
  isNotEmptyArray(value: any): boolean;
  isNotEmptyObject(value: any): boolean;
  isNotEmptyString(value: any): boolean;
  isNull(value: any): boolean;
  isNumber(value: any): boolean;
  isObject(value: any): boolean;
  isRegExp(value: any): boolean;
  isString(value: any): boolean;
  isUndefined(value: any): boolean;
};

declare const DataValidator: DataValidator;

export default DataValidator;
export {
  compare,
  isArray,
  isBlob,
  isBoolean,
  isDate,
  isEvent,
  isFile,
  isFloat,
  isFormData,
  isFunction,
  isInteger,
  isJSON,
  isNaN,
  isNotEmptyArray,
  isNotEmptyObject,
  isNotEmptyString,
  isNull,
  isNumber,
  isObject,
  isRegExp,
  isString,
  isUndefined,
};