import FormData from 'form-data';

export function compare(firstValue, secondValue) {
  if (typeof firstValue !== typeof secondValue) {
    return false;
  }

  if (isArray(firstValue) && isArray(secondValue)) {
    if (firstValue.length !== secondValue.length) {
      return false;
    }

    for (let i = 0; i < firstValue.length; i++) {
      if (!compare(firstValue[i], secondValue[i])) {
        return false;
      }
    }
  } else if (isObject(firstValue) && isObject(secondValue)) {
    const firstKeys = Object.keys(firstValue), secondKeys = Object.keys(secondValue);

    if (firstKeys.length !== secondKeys.length) {
      return false;
    }

    for (let i = 0; i < firstKeys.length; i++) {
      if (firstKeys[i] !== secondKeys[i]) {
        return false;
      }

      if (!compare(firstValue[firstKeys[i]], secondValue[secondKeys[i]])) {
        return false;
      }
    }
  } else if (isJSON(firstValue) && isJSON(secondValue)) {
    try {
      const first = JSON.parse(firstValue), second = JSON.parse(secondValue);

      if (!compare(first, second)) {
        return false;
      }
    } catch (e) {
      return false;
    }
  } else if (
    (isNaN(firstValue) && !isNaN(secondValue)) ||
    (!isNaN(firstValue) && isNaN(secondValue))
  ) {
    return false;
  } else if (firstValue !== secondValue) {
    return false;
  }

  return true;
}

export function isArray(value) {
  return Array.isArray(value) === true;
}

export function isBlob(value) {
  return (
    isObject(value) &&
    isNumber(value.size) &&
    isString(value.type) &&
    isFunction(value.slice)
  ) === true;
}

export function isBoolean(value) {
  return typeof value === 'boolean';
}

export function isDate(value) {
  return (
    value instanceof Date &&
    !isNaN(value.valueOf())
  ) === true;
}

export function isEvent(value) {
  return !!(value && value.stopPropagation && value.preventDefault);
}

export function isFile(value) {
  return (
    isBlob(value) &&
    (isObject(value.lastModifiedDate) || isNumber(value.lastModified)) &&
    isString(value.name)
  ) === true;
}

export function isFloat(value) {
  return (
    isNumber(value) &&
    !isInteger(value) &&
    Number.parseInt(value.toString()) !== Number.parseFloat(value.toString())
  ) === true;
}

export function isFormData(value) {
  return value instanceof FormData;
}

export function isFunction(value) {
  return typeof value === 'function';
}

export function isInteger(value) {
  return (
    isNumber(value) &&
    Number.isInteger(value) &&
    Number.parseInt(value.toString(), 10) === value &&
    Number(value) % 1 === 0
  ) === true;
}

export function isJSON(value) {
  if (isString(value)) {
    try {
      const json = JSON.parse(value);

      return (isObject(json) || isArray(json)) === true;
    } catch (e) {
    }
  }

  return false;
}

export function isNaN(value) {
  return Number.isNaN(Number(value));
}

export function isNotEmptyArray(value) {
  return (
    isArray(value) &&
    value.length > 0
  ) === true;
}

export function isNotEmptyObject(value) {
  return (
    isObject(value) &&
    Object.keys(value).length > 0
  ) === true;
}

export function isNotEmptyString(value) {
  return (
    isString(value) &&
    value.replace(/\s/g, '').length > 0
  ) === true;
}

export function isNull(value) {
  return null === value;
}

export function isNumber(value) {
  return typeof value === 'number';
}

export function isObject(value) {
  return (
    typeof value === 'object' &&
    !isArray(value) &&
    !isNull(value) &&
    !isFunction(value)
  ) === true;
}

export function isRegExp(value) {
  return Object.prototype.toString.call(value) === '[object RegExp]';
}

export function isString(value) {
  return typeof value === 'string';
}

export function isUndefined(value) {
  return typeof value === 'undefined';
}

const DataValidator = {
  compare,
  isArray,
  isBlob,
  isBoolean,
  isDate,
  isEvent,
  isFile,
  isFloat,
  isFormData,
  isFunction,
  isInteger,
  isJSON,
  isNaN,
  isNotEmptyArray,
  isNotEmptyObject,
  isNotEmptyString,
  isNull,
  isNumber,
  isObject,
  isRegExp,
  isString,
  isUndefined,
};

export default DataValidator;